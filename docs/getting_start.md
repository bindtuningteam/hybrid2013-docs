BindTuning SharePoint Hybrid 2013 themes include two different packages, each for installing and configuring your theme in two different CMSs: SharePoint 2013 and Office 365. 

Choose where you want to install and configure your theme first:

<a href="http://bindtuning-sharepoint-2013-themes.readthedocs.io/en/latest/">Install and configure the theme in SharePoint 2013</a>

<a href="http://bindtuning-sharepoint-office-365-themes.readthedocs.io/">Install and configure the theme in Office 365</a>

